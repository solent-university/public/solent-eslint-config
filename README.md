# Solent Eslint Config

Configuration for ESLint. The configuration customises the (https://github.com/eslint/eslint/blob/master/conf/eslint-recommended.js)[eslint:recommended] to add our own style which includes stricter linting, and uses the babel eslint parser.

## Peer dependencies

- (https://www.npmjs.com/package/@babel/eslint-parser)[@babel/eslint-parser]
- (https://www.npmjs.com/package/@babel/eslint-plugin)[@babel/eslint-plugin]
- (https://www.npmjs.com/package/eslint)[eslint]