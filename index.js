module.exports = {
    extends: ['eslint:recommended'],
    env: {
        browser: true,
        node: true,
        jest: true,
        es6: true
    },
    parser: '@babel/eslint-parser',
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
            modules: true,
            experimentalObjectRestSpread: true
        }
    },
    /**
     * Rule enforcement:
     *  0 - off
     *  1 - warning
     *  2 - error
     */
    rules: {
        /* eslint rules */
        'array-bracket-spacing': [2, 'never'],
        'arrow-parens': [2, 'as-needed'],
        'arrow-spacing': [2, { before: true, after: true }],
        'camelcase': 2,
        'comma-dangle': [2, 'never'],
        'eqeqeq': [2, 'smart'],
        'jsx-quotes': [2, 'prefer-double'],
        'key-spacing': [
            2,
            {
                afterColon: true,
                beforeColon: false,
                mode: 'strict'
            }
        ],
        'lines-between-class-members': [
            2,
            'always',
            {
                exceptAfterSingleLine: true
            }
        ],
        'max-len': [
            2,
            {
                code: 110,
                ignoreComments: true,
                ignoreStrings: true,
                ignoreTemplateLiterals: true
            }
        ],
        'multiline-ternary': 0,
        'no-alert': 0,
        'no-async-promise-executor': 0,
        'no-console': [
            2,
            {
                allow: ['info', 'error']
            }
        ],
        'no-debugger': 0,
        'no-multi-spaces': 2,
        'no-multiple-empty-lines': [2, { max: 1 }],
        'no-prototype-builtins': 0,
        'no-unused-expressions': [2, { allowTernary: true, allowShortCircuit: true }],
        'no-unused-vars': [2, { ignoreRestSiblings: true }],
        'no-whitespace-before-property': 2,
        'object-curly-spacing': [2, 'always'],
        'padded-blocks': [2, 'never'],
        'quotes': [
            2,
            'single',
            {
                avoidEscape: true,
                allowTemplateLiterals: true
            }
        ],
        'require-atomic-updates': 0,
        'semi': [2, 'always'],
        'space-infix-ops': 2,
        'space-before-blocks': 2,
        'valid-typeof': 2
    }
};
